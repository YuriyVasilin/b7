<?php
extract($_POST);
try
{
    require 'db.php';
	
    $login = $_POST['save'];
	$token = $_POST['token'];
	
    if ($token=="must_be_checked"){
        $sth = $db->prepare("DELETE FROM backend6 WHERE login=:login");
        $sth->bindParam(':login', $login);
        $sth->execute();
    }

    header('Location: admin.php');
}
catch(PDOException $e)
{
    print ('Error : ' . $e->getMessage());
    exit();
}